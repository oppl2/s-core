// Package main is just an entry point.
package main

import (
	"fmt"

	"s-core/cgroups"
)

func main() {
	fmt.Println("hello world")
	cgroups.Virtualization()
}
