package slog

import (
	"os"

	"github.com/sirupsen/logrus"
)

var logger = logrus.New()

func init() {
	logger.SetFormatter(&logrus.JSONFormatter{})
	logger.SetOutput(os.Stdout)
	handler := func() {
		// gracefully shutdown something...
		logrus.Errorln("gracefully shutdown something...")
	}
	logrus.RegisterExitHandler(handler)
}

// func New() *logrus.Logger {
// 	return logger
// }
