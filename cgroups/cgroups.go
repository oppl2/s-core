package cgroups

import (
	"errors"
	"os"
	"os/exec"
	"strings"
	"syscall"
	"time"

	cv2 "github.com/containerd/cgroups/v2"
	"github.com/sirupsen/logrus"
	"golang.org/x/sys/unix"
)

const (
	subtreeControl     = "cgroup.subtree_control"
	controllersFile    = "cgroup.controllers"
	defaultCgroup2Path = "/sys/fs/cgroup"
	defaultSlice       = "system.slice"
	realtimeSlice      = "/realtime.slice"
	rteService         = "sinsegyeRTE.service"
)

const (
	programeRTE = "./a.out"
)

// var slog = s.New()

func init() {
	checkCgroupMode()
}

// Virtualization device
func Virtualization() {
	m, err := createNewManager()
	if err != nil {
		os.Exit(-1)
	}
	pid := runRTE()
	if pid <= 0 {
		os.Exit(-1)
	}
	// m, err := cv2.NewSystemd(realtimeSlice, "/programeRTE", pid, ctrlResouce())
	// if err != nil {
	// 	os.Exit(-1)
	// }
	evCh, errCh := m.EventChan()
	m.AddProc(uint64(pid))

	time.Sleep(500 * time.Second)
	done := false
	for !done {
		select {
		case <-evCh:
		case err := <-errCh:
			if err != nil {
				logrus.Fatalf("Unexpected error on error channel: %v", err)
			}
			done = true
		case <-time.After(5 * time.Second):
			logrus.Fatal("Timed out")
		}
	}
	time.Sleep(500 * time.Second)
}

func checkCgroupMode() {
	var st syscall.Statfs_t

	if err := syscall.Statfs(defaultCgroup2Path, &st); err != nil {
		logrus.Fatalln("cannot statfs cgroup root")
	}
	isUnified := st.Type == unix.CGROUP2_SUPER_MAGIC
	if !isUnified {
		logrus.Fatalln("System running in hybrid or cgroupv1 mode")
	}
}

func runRTE() (pid int) {
	l := logrus.WithField("RTE exec", programeRTE)
	if _, err := os.Stat(programeRTE); errors.Is(err, os.ErrNotExist) {
		l.Errorln("File not exists")
		return
	}
	cmd := exec.Command(programeRTE)
	if err := cmd.Start(); err != nil {
		l.Errorf("Failed to start process: %v", err)
		return
	}
	proc := cmd.Process
	if proc == nil {
		l.Errorf("Get process failed!")
		return
	}
	logrus.WithField("Pid", cmd.Process.Pid).Debugf("start %s success!", programeRTE)
	return cmd.Process.Pid
}

func createNewManager() (m *cv2.Manager, err error) {
	groupPath := strings.Join([]string{
		realtimeSlice,
		rteService,
		// fmt.Sprintf("%s-%d", "programeRTE", os.Getpid()),
	}, "/")
	// m, err = loadManager(groupPath)
	// if err == nil {
	// 	return
	// }
	m, err = cv2.NewManager(defaultCgroup2Path, groupPath, ctrlResouce())
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"defaultCgroup2Path": defaultCgroup2Path,
			"groupPath":          groupPath,
		}).Errorf("Create new managet failed! %v", err)
	}
	return
}

func loadManager(group string) (*cv2.Manager, error) {
	m, err := cv2.LoadManager(defaultCgroup2Path, group)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"defaultCgroup2Path": defaultCgroup2Path,
			"group":              group,
		}).Debugf("Not found cgroup %v", err)
	}
	return m, err
}

func ctrlResouce() (res *cv2.Resources) {
	res = &cv2.Resources{}
	res.CPU = ctrlCPU()
	return
}

func ctrlCPU() (cpu *cv2.CPU) {
	var (
		quota  int64  = 10000
		period uint64 = 8000
		weight uint64 = 100
	)
	return &cv2.CPU{
		Weight: &weight,
		Max:    cv2.NewCPUMax(&quota, &period),
		Cpus:   "3",
		Mems:   "0",
	}
}
