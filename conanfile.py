import os
from conans import ConanFile, AutoToolsBuildEnvironment, tools
from conans.errors import ConanInvalidConfiguration
from conans.tools import os_info, SystemPackageTool
import contextlib
import semver
required_conan_version = ">=1.33.0"

def get_version():
    git = tools.Git()
    try:
        # If the last commit is not a tag, git.get_tag() returns None
        if git.get_tag() != None:
            return git.get_tag()
        else:
            return ("%s_%s" % (git.get_branch(), git.get_revision()[0:7])).replace(' ','').replace('(','').replace(')','')
    except:
        return ""

class SCOREConan(ConanFile):
    name = "s-core"
    branch = "master"
    url = "https://gitlab.com/oppl2/SCORE"
    generators = "make"
    settings = "os", "arch", "compiler", "build_type"
    _autotools = None
    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "no_main": [True, False],
    }
    default_options = {
        "shared": False,
        "fPIC": True,
        "no_main": False,
    }

    @property
    def _cmake_install_base_path(self):
        return "cmake"
    def init(self):
        try:
            self._semver = semver.semver(get_version(),False)
        except:
            self._semver = semver.semver("0.0.0",False)
        self.version = self._semver.version
    def _configure_autotools(self):
        if self._autotools:
            return self._autotools
        self._autotools = AutoToolsBuildEnvironment(self)
        return self._autotools
    @contextlib.contextmanager
    def _build_context(self):
        with tools.environment_append(tools.RunEnvironment(self).vars):
            yield

    def source(self):
        self.run("git clone https://GIT_ACCESS_TOKEN:%s@gitlab.com/oppl2/SCORE.git && cd s-core && git checkout %s" %
                 (tools.get_env("GIT_ACCESS_TOKEN"), self.branch))

    # def requirements(self):

    def build(self):
        with tools.chdir(self.source_folder):
            self.run("pwd")
            autotools = self._configure_autotools()
            env_build_vars = autotools.vars
            env_build_vars['BIN'] = 'build/%s_%s/bin' % (self.settings.arch,self.settings.build_type)
            autotools.make(target="clean")
            autotools.make(vars=env_build_vars)

    def package(self):
        self.copy("*", src="bin", dst="bin")
        self.copy("*", src="lib",dst="lib")

    def imports(self):
        self.copy("*.so*", src="", dst="")

    def deploy(self):
        self.copy("*", src="bin", dst="bin")
        self.copy("*", src="lib", dst="lib")




